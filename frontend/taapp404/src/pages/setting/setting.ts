import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { FormLoginPage } from '../form-login/form-login';
/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  apps:any;
  constructor(public nav:Nav, public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }

  goFormLogin(){
  	this.storage.remove('uid');
  	this.nav.setRoot(FormLoginPage);
  }

  linkRouting(sLink:any,data:any){
  	this.navCtrl.push(sLink,{data:data});
  }

}
