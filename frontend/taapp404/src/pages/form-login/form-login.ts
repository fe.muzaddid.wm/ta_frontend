import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,Nav } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';

import { DaftarPage } from '../daftar/daftar';
import { FooterPage } from '../footer/footer';

@IonicPage()
@Component({
  selector: 'page-form-login',
  templateUrl: 'form-login.html',
})
export class FormLoginPage {
	loginForm: FormGroup;
  validation_message = {
    'email' : [
         {type:'required', message:'Email Tidak Boleh Kosong.'},
         {type:'pattern', message:'Email tidak sesuai format.'}
    ],
    'password' : [
         {type:'required', message:'Password tidak boleh kosong.'}
    ]
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private alertCtrl: AlertController, public statusBar: StatusBar, public storage: Storage,public nav:Nav) {
  	 this.loginForm = formBuilder.group({
      email:['',  Validators.compose([
                                  Validators.required,
                                  Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
      password:['', Validators.required]
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormLoginPage');
  }
  ionViewDidEnter(){
    this.storage.get('uid').then((uid)=>{
      if(uid!=null){
        this.linkRouting(FooterPage,null);
      }
    });
  }
  
  goToLogin(){
    if(this.loginForm.valid){
      this.storage.set("uid",this.loginForm.value.email);
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#ffffff');
      this.nav.setRoot(FooterPage);
    }else{
      this.presentAlert("Oops!","Tidak dapat memproses, Perbaiki data yang kamu masukkan");
    }
  }

  goDaftarBaru(){
    this.linkRouting(DaftarPage,null);
  }

  goLupaPassword(){
    this.presentAlert("Lupa Password?","Jika anda lupa password silahkan hubungi call center +62087809097912");
  }

  presentAlert(tittle,desc) {
  let alert = this.alertCtrl.create({
    title: tittle,
    subTitle: desc,
    buttons: ['Dismiss']
  });
  alert.present();
  }

  linkRouting(sLink:any,data:any){
    this.navCtrl.push(sLink,{data:data});
  }
}
