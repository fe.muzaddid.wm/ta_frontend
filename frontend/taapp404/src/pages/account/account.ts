import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { SettingPage } from '../setting/setting';
/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public statusBar: StatusBar) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }
  ionViewDidEnter(){
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#ffffff');
  }

  goSettingAccount(){
      this.statusBar.styleBlackTranslucent();
      this.statusBar.backgroundColorByHexString('#e32413');
    this.linkRouting(SettingPage,null);
  }

  linkRouting(sLink:any,data:any){
    this.navCtrl.push(sLink,{data:data});
  }
}
