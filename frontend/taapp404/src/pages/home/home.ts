import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';

import { DepositPage } from '../deposit/deposit';
import { PembelianPage } from '../pembelian/pembelian';
import { PembayaranPage } from '../pembayaran/pembayaran';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private dLogin:any;
  constructor(public navCtrl: NavController,private storage:Storage,private statusBar:StatusBar) {

  }
  
  ionViewDidEnter(){
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#ffffff');
  }

  goDepositPage(){
    this.statusBar.styleBlackTranslucent();
    this.statusBar.backgroundColorByHexString('#e32413');
  	this.linkRouting(DepositPage,null);
  }

  goPembelianPage(){
    this.statusBar.styleBlackTranslucent();
    this.statusBar.backgroundColorByHexString('#e32413');
  	this.linkRouting(PembelianPage,null);
  }
  goPembayaranPage(){
    this.statusBar.styleBlackTranslucent();
    this.statusBar.backgroundColorByHexString('#e32413');
  	this.linkRouting(PembayaranPage,null);
  }

  linkRouting(sLink:any,data:any){
  	this.navCtrl.push(sLink,{data:data});
  }
}
