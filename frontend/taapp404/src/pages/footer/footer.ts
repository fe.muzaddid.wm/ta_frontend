import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { MessagePage } from '../message/message';
import { AccountPage } from '../account/account';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the FooterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  templateUrl: 'footer.html'
})
export class FooterPage {
  pageHome = HomePage;
  pageMassage = MessagePage;
  pageAccount = AccountPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}