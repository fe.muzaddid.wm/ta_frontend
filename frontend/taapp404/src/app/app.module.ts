import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { FormLoginPage } from '../pages/form-login/form-login';
import { MessagePage } from '../pages/message/message';
import { AccountPage } from '../pages/account/account';
import { FooterPage } from '../pages/footer/footer';
import { DaftarPage } from '../pages/daftar/daftar';
import { SettingPage } from '../pages/setting/setting';
import { DepositPage } from '../pages/deposit/deposit';
import { PembelianPage } from '../pages/pembelian/pembelian';
import { PembayaranPage } from '../pages/pembayaran/pembayaran';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    FormLoginPage,
    MessagePage,
    AccountPage,
    FooterPage,
    DaftarPage,
    SettingPage,
    DepositPage,
    PembelianPage,
    PembayaranPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    FormLoginPage,
    MessagePage,
    AccountPage,
    FooterPage,
    DaftarPage,
    SettingPage,
    DepositPage,
    PembelianPage,
    PembayaranPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
